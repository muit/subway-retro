// Copyright 2018 Miguel Fernández Arce. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "OwnedObject.generated.h"

/**
 * Allows calling world context functions from child BPs classes
 * Even though C++ doesn't seem to be allowed, this small object was necessary.
 */
UCLASS(Blueprintable)
class SUBWAYRETRO_API UOwnedObject : public UObject
{
	GENERATED_BODY()


public:

	UFUNCTION(BlueprintCallable, Category = OwnedObject)
	void Destroy() {
		OnDestroyed();
		MarkPendingKill();
	}

	UFUNCTION(BlueprintPure, Category = OwnedObject, meta = (DisplayName="Get Outer"))
	FORCEINLINE UObject* BP_GetOuter() const { return GetOuter(); }

	/** Event that gets called when the objects is destroyed MANUALLY. GC will not call it */
	UFUNCTION(BlueprintImplementableEvent, Category = OwnedObject)
	void OnDestroyed();


protected:

	virtual UWorld* GetWorld() const override;
};
