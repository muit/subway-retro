// Copyright 2018 Miguel Fernández Arce. All Rights Reserved.

#include "SubwayRetro.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SubwayRetro, "SubwayRetro" );
