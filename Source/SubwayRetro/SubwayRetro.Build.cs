// Copyright 2018 Miguel Fernández Arce. All Rights Reserved.

using UnrealBuildTool;

public class SubwayRetro : ModuleRules
{
	public SubwayRetro(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"});
	}
}
