// Copyright 2018 Miguel Fernández Arce. All Rights Reserved.

#include "OwnedObject.h"


UWorld* UOwnedObject::GetWorld() const
{
	// If we are a CDO, we must return nullptr instead of calling Outer->GetWorld() to fool UObject::ImplementsGetWorld.
	if (HasAllFlags(RF_ClassDefaultObject))
		return nullptr;

	return GetOuter()->GetWorld();
}
