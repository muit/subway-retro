// Copyright 2018 Miguel Fernández Arce. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class SubwayRetroTarget : TargetRules
{
	public SubwayRetroTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.Add("SubwayRetro");
	}
}
