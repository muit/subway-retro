// Copyright 2018 Miguel Fernández Arce. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class SubwayRetroEditorTarget : TargetRules
{
	public SubwayRetroEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.Add("SubwayRetro");
	}
}
