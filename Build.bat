@echo off
setlocal EnableDelayedExpansion
set arg1=%1
set arg2=%2


set build_path=%cd%\Build

set engine_path=%programfiles%\Epic Games\UE_4.21

echo.
echo "**************************"
echo "* BUILD MANAGER *"
echo "**************************"
echo.

if "%arg1%" EQU "/h" (
    echo [/b to build][/d to deploy]
    exit
)

rem If there is any argument
if not "%arg1%" EQU "" ( goto :commands )

:build_questions
echo Engine directory [!engine_path!]
set /p new_engine_path=: 
if not "%new_engine_path%" EQU "" (
    set engine_path=%new_engine_path%
)

echo.
echo Build path [/Build]
set /p new_path=: 
if not "%new_path%" EQU "" (
    set build_path=%new_path%
)
echo.
goto :build

:commands
set running_commands=y

if "%arg1%" EQU "/b" ( goto :build )
goto :exit


:build
echo.
echo Running build...
echo.

set batchfiles_path=!engine_path!\Engine\Build\BatchFiles
call "%batchfiles_path%\RunUAT" BuildCookRun -project="%cd%\SubwayRetro.uproject" -noP4 -platform=Win64 -clientconfig=Development -serverconfig=Development -cook -allmaps -build -stage -pak -archive -archivedirectory="%build_path%"  && (
  echo.
) || (
  echo.
  echo ERROR: Couldn't build the project. Check the logs below
  goto :exit
)

echo.

:exit
if not defined running_commands (
pause>nul
)
exit
